//
//  ViewController.swift
//  Let's Go Scavenge
//
//  Created by James Koenig on 8/29/18.
//  Copyright © 2018 James Koenig. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        
        guard let trackingObjects = ARReferenceObject.referenceObjects(inGroupNamed: "AR Resources", bundle: nil) else {
            print("Could not load images")
            return
        }
        
        // Setup Configuration
        configuration.detectionObjects = trackingObjects
        
        sceneView.session.run(configuration, options: .removeExistingAnchors)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    func createTextNode(string: String) -> SCNNode {
        let text = SCNText(string: string, extrusionDepth: 0.1)
        text.font = UIFont.systemFont(ofSize: 1.0)
        text.flatness = 0.01
        text.firstMaterial?.diffuse.contents = UIColor.white
        
        let textNode = SCNNode(geometry: text)
        
        let fontSize = Float(0.04)
        textNode.scale = SCNVector3(fontSize, fontSize, fontSize)
        
        return textNode
    }
    
    // MARK: - ARSCNViewDelegate
    

    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        if let objectAnchor = anchor as? ARObjectAnchor {
            if objectAnchor.referenceObject.name == "SodaStream" {
                let boxNode = createTextNode(string: "Gas or Carbonated? Which is it?")
                node.addChildNode(boxNode)
            } else if objectAnchor.referenceObject.name == "Juicer" {
                let boxNode = createTextNode(string: "Some carrots a day, keeps the doctor away.")
                node.addChildNode(boxNode)
            } else if objectAnchor.referenceObject.name == "FrenchPress" {
                let boxNode = createTextNode(string: "Some like it hot, some like it cold.")
                node.addChildNode(boxNode)
            }
        }
        
        return node
    }

    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
